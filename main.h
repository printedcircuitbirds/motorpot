#ifndef MAIN_H_
#define MAIN_H_

#define DEF_API_USART 0
#define DEF_API_I2C 1ul
#define DEF_API_SPI 0

#define R12 4300ul
#define R10 10000ul //POT
#define R11 82ul

//The ADC result for VCC~4.3V with VREF = 0.55V
#define VREF_VOLTAGE (4300ul*R11*1024/550/(R10+R11+R12))

#define ADC_MUXPOS_POTLOW_gc    ADC_MUXPOS_AIN1_gc
#define ADC_MUXPOS_POTHIGH_gc   ADC_MUXPOS_AIN2_gc
#define ADC_MUXPOS_POTWIPER_gc  ADC_MUXPOS_AIN3_gc
#define API_RESOLUTION          1023ul

typedef enum {
    POT_RK168 = 0,   // RK168 has 3B taper characteristic, and the 8-bit DAC resolution cannot handle it
    POT_PSx = 1,     // PSM60, PSM01, PSL60, PSL01
} pot_t;

#endif /* MAIN_H_ */