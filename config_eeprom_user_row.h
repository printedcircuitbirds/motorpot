/*
 * new_tinymegaAVR eeprom and user row settings
 *
 */

#ifndef _CONFIG_EEPROM_USER_ROW_H_
#define _CONFIG_EEPROM_USER_ROW_H_
#include <avr/io.h>
#include <stdbool.h>
#include "main.h"

/* Structs to be configured for code friendly memory locations */
typedef struct eeprom_struct {
	uint8_t data;
	uint8_t flags;
} eeprom_data_t;

typedef struct userrow_struct {
	pot_t pot_type;
	uint8_t touch;
	uint8_t sample100ms;
	uint16_t wiper_target;
} userrow_data_t;

/* Pointers for use in the application */
eeprom_data_t *eeprom = (eeprom_data_t *) EEPROM_START;
userrow_data_t *userrow = (userrow_data_t *) USER_SIGNATURES_START;

/* The initial values to put in eeprom and user row for the elf file */
eeprom_data_t eeprom_init __attribute((__used__, __section__(".eeprom"))) = {
	.data = 0x01,
	.flags = 0x02,
};

userrow_data_t userrow_init __attribute((__used__, __section__(".user_signatures"))) = {
	.pot_type = POT_RK168,
	.touch = 0,
	.sample100ms = 10,
	.wiper_target = 511,
};

/* Function to check if the NVM can be updated */
static inline bool eeprom_userrow_write_busy(bool wait)
{
	// You did something wrong..
	if(NVMCTRL.STATUS & NVMCTRL_WRERROR_bm) {
		return true;
	}

	// Do you want to wait until ready or keep polling?
	if(wait) {
		while(NVMCTRL.STATUS & (NVMCTRL_EEBUSY_bm | NVMCTRL_FBUSY_bm));
	} else if(NVMCTRL.STATUS) {
		return true;
	}

	// We are ready to write!
	return false;
}

/* Function to write changes to eeprom or user row page buffer
 * Make sure to only change the content of one page at the time */
static inline bool eeprom_userrow_write_buffer(bool blocking)
{
	// NVM is busy or you messed up..
	if(eeprom_userrow_write_busy(false)) {
		return true;
	}

	// The actual write command
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);

	//Do you want to wait until finished?
	if(blocking) {
		while(NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);
	}

	// We are done!
	return false;
}

#endif // _CONFIG_EEPROM_USER_ROW_H_