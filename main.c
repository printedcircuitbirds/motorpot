#include <atmel_start.h>
#include <util/delay.h>

#include "main.h"
#include "config_eeprom_user_row.h"
#include "rk168.h"

extern volatile uint8_t measurement_done_touch;

volatile uint16_t measurements[3] = {0};
volatile uint16_t wiper = 0;
volatile float magic_number = 0;
volatile uint16_t CCL_WIPER_countdown = 0;

volatile uint16_t calibrate_low[4] = {0};
volatile uint16_t calibrate_high[4] = {0};

volatile userrow_data_t config = {0};
volatile struct {
    uint8_t touch;
    uint16_t sample_ms;
    uint8_t move_wiper;
    uint8_t new_measurement;
    uint16_t last_sample;
} status = {
    .touch = 0,
    .sample_ms = 10 * 100,
    .move_wiper = true,
    .new_measurement = false,
};

uint16_t ADC_get_wiper_position(void);
void CCL_WIPER_move(uint16_t target);

int main(void)
{
    /* Initializes MCU, drivers and middleware */
    atmel_start_init();

    // TODO: Make this one a bit more generic
    config.pot_type = userrow->pot_type;
    config.touch = userrow->touch;
    config.sample100ms = userrow->sample100ms;
    config.wiper_target = userrow->wiper_target;

#if DEF_TOUCH_DATA_STREAMER_ENABLE == 0
#if DEF_API_USART == 1
    USART_0_initialization();
#elif DEF_API_I2C == 1
    I2C_0_initialization();
#elif DEF_API_SPI == 1
    SPI_0_initialization();
#else
#error "Need to define comms interface"
#endif
#else
#warning "Be aware that you are running with TOUCH_DATA_STREAMER"
#endif

    // Initial ADCREF = 0.55V, used to figure out if VCC is 5V or 3V3
    uint16_t res_low = ADC_get_conversion(ADC_MUXPOS_POTLOW_gc) / 64;
    if(res_low < VREF_VOLTAGE) {
        VREF.CTRLA |= VREF_DAC0REFSEL_2V5_gc;
        VREF.CTRLC |= VREF_ADC1REFSEL_2V5_gc;
        magic_number = 3.3 / (R10 + R11 + R12) * 255.0 / 2.5;
    }
    else {
        VREF.CTRLA |= VREF_DAC0REFSEL_4V34_gc;
        VREF.CTRLC |= VREF_ADC1REFSEL_4V34_gc;
        magic_number = 5.0 / (R10 + R11 + R12) * 255.0 / 4.34;
    }

    wiper = ADC_get_wiper_position();
    sei();

// This was used for creating the LUT for the RK168 pot
//     uint16_t i = 0;
//     PORTC.OUTSET = PIN1_bm;
//     while(i++ < 500) {
//         PORTB.OUTCLR = PIN4_bm;
//         _delay_ms(18);
//         PORTB.OUTSET = PIN4_bm;
//         _delay_ms(300);
//         wiper = ADC_get_wiper_position();
//     }
//     PORTB.OUTCLR = PIN4_bm;
//     PORTC.OUTCLR = PIN1_bm;
//     while(1);

// This might be used for calibrating the PSM pot
//     CCL_WIPER_move(100);
//     _delay_ms(1000);
//     CCL_WIPER_move(0);
//     DAC_WIPER_set_output(0); // Override target
//     _delay_ms(1000);
//
//     //TODO: Sweep DAC up until AC0 state toggles
//     while(AC0.STATUS & AC_STATE_bm) {
//         DAC0.DATA++;
//         _delay_ms(1);
//     }
//
//     ADC_get_wiper_position();
//     calibrate_low[0] = measurements[0];
//     calibrate_low[1] = measurements[1];
//     calibrate_low[2] = measurements[2];
//     calibrate_low[3] = DAC0.DATA;
//
//     CCL_WIPER_move(900);
//     _delay_ms(1000);
//     CCL_WIPER_move(1023);
//     DAC_WIPER_set_output(255); // Override target
//     _delay_ms(1000);
//
//     //TODO: Sweep DAC down until AC0 state toggles
//     while(!(AC0.STATUS & AC_STATE_bm)) {
//         DAC0.DATA--;
//         _delay_ms(1);
//     }
//
//     ADC_get_wiper_position();
//     calibrate_high[0] = measurements[0];
//     calibrate_high[1] = measurements[1];
//     calibrate_high[2] = measurements[2];
//     calibrate_high[3] = DAC0.DATA;
//
//     CCL_WIPER_move(wiper);

    while(1) {
        if(status.new_measurement) {
            status.new_measurement = false;

            wiper = ADC_get_wiper_position();
            if(wiper != status.last_sample) {
                status.last_sample = wiper;

                // TODO: Send new wiper position
            }
        }

        if(status.move_wiper) {
            status.move_wiper = false;

            if(!status.touch) {
                CCL_WIPER_move(config.wiper_target);
            }
        }

        if(config.touch) {
            touch_process();
            if(measurement_done_touch == 1) {
                measurement_done_touch = 0;
                if(get_sensor_state(0) & KEY_TOUCHED_MASK) {
                    // Ignore incoming updates
                    status.touch = true;

                    // Release motor in case it is running
                    CCL_WIPER_countdown = 1;

                    // Optional TODO: Send touch detected?

                }
                else if(status.touch) {
                    // Accept incoming updates
                    status.touch = false;

                    // Sample 50ms after a touch is released
                    status.sample_ms = 50;

                }
            }
        }
    }
}

uint16_t ADC_get_wiper_position(void)
{
    uint32_t output = 0;

    // Read 64 accumulated samples from each channel
    measurements[0] = ADC_get_conversion(ADC_MUXPOS_POTLOW_gc);
    measurements[1] = ADC_get_conversion(ADC_MUXPOS_POTWIPER_gc);
    measurements[2] = ADC_get_conversion(ADC_MUXPOS_POTHIGH_gc);

    if(config.pot_type == POT_RK168) {
        output = get_rk168_3v3_position(measurements[1]);
    }
    else {
        // Convert measurements to a 10-bit wiper position
        if(measurements[0] > measurements[1]) {
            output = 0;
        }
        else if(measurements[1] >= measurements[2]) {
            output = API_RESOLUTION;
        }
        else {
            output = ((uint32_t)(measurements[1] - measurements[0]) * API_RESOLUTION) / (measurements[2] - measurements[0]);
        }
    }
    return output;
}

void CCL_WIPER_move(uint16_t target)
{
    // Calculate wiper target from 10-bit range
    uint8_t dac_val = ((R10 * (float)target / API_RESOLUTION) + R11) * magic_number;
    DAC_WIPER_set_output(dac_val);

    if(config.pot_type == POT_RK168) {
        volatile uint16_t adc_target = get_rk168_3v3_target(target);

        // Run (slow) motor
        // Controlled only by measuring the wiper channel with the ADC
        // ACs will probably trigger, but it will not do anything
        MOTOR_IN2_set_level(true);
        MOTOR_IN1S_set_level(true);
        uint8_t countdown = 3;
        while(countdown) {
            measurements[1] = ADC_get_conversion(ADC_MUXPOS_POTWIPER_gc);
            if(adc_target < measurements[1]) {
                if(MOTOR_IN2_get_level()) {
                    countdown--;
                    MOTOR_IN2_set_level(false);
                    MOTOR_IN1S_set_level(true);
                }
            }
            else {
                if(MOTOR_IN1S_get_level()) {
                    countdown--;
                    MOTOR_IN1S_set_level(false);
                    MOTOR_IN2_set_level(true);
                }
            }
            _delay_ms(20);
        }
        MOTOR_IN2_set_level(false);
        MOTOR_IN1S_set_level(false);
    }
    else {
        // Start motor
        TCA0.SINGLE.CMP2BUF = 1023;
        CCL.CTRLA = CCL_ENABLE_bm;
        CCL_WIPER_countdown = 0;
    }
}

ISR(RTC_PIT_vect)
{
    // 1ms period for a lot of stuff
    if(status.sample_ms) {
        if(--status.sample_ms == 0) {
            status.new_measurement = true;

            status.sample_ms = 100ul * config.sample100ms;
        }
    }

    // When to stop moving the potmeter
    if(CCL_WIPER_countdown) {
        if(--CCL_WIPER_countdown == 0) {
            // Stop motor
            CCL.CTRLA = 0;
            // Clear AC flags just in case
            AC0.STATUS = AC_CMP_bm;
            AC1.STATUS = AC_CMP_bm;
            AC2.STATUS = AC_CMP_bm;
        }
    }

    // Slow down fast potmeters
    if(config.pot_type == POT_PSx) {
        if(TCA0.SINGLE.CMP2 > 800) {
            TCA0.SINGLE.CMP2BUF = TCA0.SINGLE.CMP2 - 2;
        }
    }

    if(config.touch) {
        // The touch timer handles time from 1ms ticks
        touch_timer_handler();
    }

    /* The interrupt flag has to be cleared manually */
    RTC.PITINTFLAGS = RTC_PI_bm;
}

ISR(AC0_AC_vect)
{

    if(!CCL_WIPER_countdown) {
        // Run motor for ~80ms after first hit
        CCL_WIPER_countdown = 80;
    }

    /* The interrupt flag has to be cleared manually */
    AC0.STATUS = AC_CMP_bm;
}

ISR(AC1_AC_vect)
{

    if(!CCL_WIPER_countdown && (AC0.STATUS & AC_STATE_bm)) {
        // Run motor for ~250ms after first hit
        CCL_WIPER_countdown = 250;
    }

    /* The interrupt flag has to be cleared manually */
    AC1.STATUS = AC_CMP_bm;
}

ISR(AC2_AC_vect)
{

    if(!CCL_WIPER_countdown && !(AC0.STATUS & AC_STATE_bm)) {
        // Run motor for ~250ms after first hit
        CCL_WIPER_countdown = 250;
    }

    /* The interrupt flag has to be cleared manually */
    AC2.STATUS = AC_CMP_bm;
}